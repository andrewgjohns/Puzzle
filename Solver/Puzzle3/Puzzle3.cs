﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Reaktor.Puzzle3
{
    public class Reading
    {
        public string readingID { get; set; }
        public List<int> reading { get; set; }
    }

    public class Region
    {
        public string regionID { get; set; }
        public List<Reading> readings { get; set; }
    }

    public class RootObject
    {
        public List<Region> regions { get; set; }
    }

    public class Flood
    {
        public string RegionId { get; set; }
        public List<string> FloodPoints { get; set; }
        public List<int> Floods { get; set; }

    }

    class Puzzle3
    {
        private RootObject GetData()
        {

            var json = File.ReadAllText(@"puzzle3\flood.json");
            var results = JsonConvert.DeserializeObject<RootObject>(json);
            return results;
        }

        public void Solve()
        {
            Console.WriteLine("Puzzle 3");
            var data = GetData();

            var floods = new List<Flood>();

            foreach (var region in data.regions)
            {
                var flood = new Flood
                {
                    RegionId = region.regionID,
                    FloodPoints = new List<string>(),
                    Floods = new List<int>()
                };


                foreach (var readings in region.readings)
                {
                    var area = 0;
                    int index = 0;
                    int number = readings.reading[0];
                    readings.reading.RemoveAt(index);
                    while (true)
                    {

                        var nextIndex = readings.reading.FindIndex(s => s >= number);
                        if (nextIndex == -1)
                        {
                            nextIndex = IndexOfNextHighestNumber(readings.reading);
                            if (nextIndex == -1) break;
                        }

                        if (nextIndex == 0)
                        {
                            number = readings.reading[0];
                            readings.reading.RemoveAt(0);
                            continue;
                        }

                        var nextNumber = readings.reading[nextIndex];

                        var lowPoint = number < nextNumber ? number : nextNumber;

                        var dataRange = readings.reading.GetRange(0, nextIndex);

                        area += (lowPoint * dataRange.Count) - dataRange.Sum();
                        readings.reading.RemoveRange(0, nextIndex + 1);
                        number = nextNumber;

                    }

                    flood.FloodPoints.Add(readings.readingID);
                    flood.Floods.Add(area);
                }

                floods.Add(flood);
            }

            foreach (var flood in floods)
            {
                var previous = int.MinValue;
                for(int f=0; f < flood.Floods.Count; f++)
                {
                    if (previous == int.MinValue)
                    {
                        previous = flood.Floods[f];
                        continue;
                    }

                    if (flood.Floods[f] - previous > 1000)
                    {
                        Console.Write($"{flood.FloodPoints[f]}");
                    }

                    previous = flood.Floods[f];
                }
            }
            Console.WriteLine();
            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
            Console.WriteLine();
        }

        private int IndexOfNextHighestNumber(List<int> sample)
        {
            var index = -1;
            var previousValue = int.MinValue;
            for (var i = 0; i < sample.Count; i++)
            {
                if (sample[i] <= previousValue) continue;
                index = i;
                previousValue = sample[i];
            }

            return index;
        }
    }
}
