﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Reaktor.Puzzle2
{
    public class Reading
    {
        public string id { get; set; }
        public int time { get; set; }

        public Dictionary<string, int> contaminants { get; set; }
    }

    public class Result
    {
        public List<Reading> readings { get; set; }
    }

    public static class Puzzle2Extensions
    {
        public static string GetAsciiStringFromBinaryString(this string binary)
        {
            var list = new List<Byte>();
            var bin = binary.Replace(" ", "");
            for (var i = 0; i < bin.Length; i += 8)
            {
                string t = bin.Substring(i, 8);

                list.Add(Convert.ToByte(t, 2));
            }

            return Encoding.ASCII.GetString(list.ToArray());
        }

        public static double StandardDeviation(this IEnumerable<double> values)
        {
            double avg = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
        }

        public static string ConvertHex(this string hexString)
        {
            try
            {
                var ascii = string.Empty;

                for (var i = 0; i < hexString.Length; i += 2)
                {
                    var hs = string.Empty;

                    hs = hexString.Substring(i, 2);
                    var decval = Convert.ToUInt32(hs, 16);
                    var character = Convert.ToChar(decval);
                    ascii += character;
                }

                return ascii;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return string.Empty;
        }
    }

    public class Puzzle2
    {
        private List<Result> GetData()
        {
            var binary = File.ReadAllText(@"puzzle2\data.bin");
            var results = JsonConvert.DeserializeObject<List<Result>>(binary.GetAsciiStringFromBinaryString());
            return results;
        }

        public void Solve()
        {
            Console.WriteLine("Puzzle 2");
            var results = GetData();
            var areaValues = new Dictionary<int, List<double>>();

            foreach (var result in results)
            {
                foreach (var reading in result.readings)
                {
                    var total = reading.contaminants.Select(c => c.Value).Sum();
                    if (! areaValues.ContainsKey(reading.time))
                    {
                        areaValues.Add(reading.time, new List<double>()); 
                    }
                    var list = areaValues[reading.time];
                    list.Add(total);
                }
            }

            foreach (var key in areaValues.Keys)
            {
                var values = areaValues[key];
                var stdDev = values.StandardDeviation();
                var max = values.OrderByDescending(i => i).First();
                var preMax = values.Where(i => i < max).OrderByDescending(i => i).First();

                if (!(max > (preMax + stdDev))) continue;

                foreach (var result in results)
                {
                    foreach (var reading in result.readings.Where(r => r.time == key && r.contaminants.Select(c => c.Value).Sum() == max))
                    {
                        Console.WriteLine("{0} - {1}", reading.id, reading.id.ConvertHex());
                    }
                }
            }
            Console.WriteLine("Press any key to continue.");
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
