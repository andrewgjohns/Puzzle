﻿namespace Reaktor
{
    class Program
    {
        static void Main(string[] args)
        {
            var puzzle1 = new Puzzle1.Puzzle1();
            var puzzle2 = new Puzzle2.Puzzle2();
            var puzzle3 = new Puzzle3.Puzzle3();

            puzzle1.Solve();
            puzzle2.Solve();
            puzzle3.Solve();

        }
    }
}
